package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	"gitlab.com/daniel673/grpc-encryption-service/encryption"
	es "gitlab.com/daniel673/grpc-encryption-service/encryption-service"

	"google.golang.org/grpc"
)

type server struct {
	es.UnimplementedEncryptionServiceServer
}

// RPC call to encrypt incoming data and returns an encryption response
// with the encrypted data or an error
func (s *server) EncryptData(ctx context.Context, in *es.EncryptionRequest) (*es.EncryptionResponse, error) {

	ciphertext, err := encryption.EncryptDataAES(in.GetPlaintext())
	if err != nil {
		return &es.EncryptionResponse{Ciphertext: nil, Error: "Failed to encrypt"}, err
	}

	return &es.EncryptionResponse{Ciphertext: ciphertext, Error: ""}, nil
}

// RPC call that decrypts a byte array and returns a decryption response
// that is the decrypted bytes data
func (s *server) DecryptData(ctx context.Context, in *es.DecryptionRequest) (*es.DecryptionResponse, error) {

	plaintext, err := encryption.DecryptDataAES(in.GetCiphertext())
	if err != nil {
		return &es.DecryptionResponse{Plaintext: nil}, err
	}

	return &es.DecryptionResponse{Plaintext: plaintext}, err
}

func main() {
	port := flag.String("port", "50055", "The port for the server to listen on")
	flag.Parse()

	fmt.Println("Encryption server listening on port", *port)

	lis, err := net.Listen("tcp", ":"+*port)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	es.RegisterEncryptionServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
