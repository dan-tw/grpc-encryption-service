package encryption

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

var key []byte = []byte(")66LExa%!8PeMtq((-BBq,%Q#M9$AQ!2")

// Takes a plaintext variable of type []byte and returns an AES encrypted
// result using the pre-specified encryption key
func EncryptDataAES(plaintext []byte) ([]byte, error) {
	ciph, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(ciph)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	str := plaintext
	data := gcm.Seal(nonce, nonce, []byte(str), nil)
	return data, nil
}

// Takes a ciphertext variable of type []byte that is encrypted using AES and the
// pre-specified cipher key and returns the plaintext equivalent unencrypted
// NOTE: AES provided data must have been encrypted using this packages encrypt function
func DecryptDataAES(ciphertext []byte) ([]byte, error) {
	ciph, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(ciph)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, err
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}

	return plaintext, err
}
