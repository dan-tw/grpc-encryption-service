// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.6.1
// source: encryption-service/encryption_service.proto

package generated

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Represents an encryption request
type EncryptionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The data to be encrypted
	Plaintext []byte `protobuf:"bytes,1,opt,name=plaintext,proto3" json:"plaintext,omitempty"`
}

func (x *EncryptionRequest) Reset() {
	*x = EncryptionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_encryption_service_encryption_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EncryptionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EncryptionRequest) ProtoMessage() {}

func (x *EncryptionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_encryption_service_encryption_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EncryptionRequest.ProtoReflect.Descriptor instead.
func (*EncryptionRequest) Descriptor() ([]byte, []int) {
	return file_encryption_service_encryption_service_proto_rawDescGZIP(), []int{0}
}

func (x *EncryptionRequest) GetPlaintext() []byte {
	if x != nil {
		return x.Plaintext
	}
	return nil
}

// Represents an encryption response
type EncryptionResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The error message if encryption failed
	// (This will be an empty string if encryption succeeded)
	Error string `protobuf:"bytes,1,opt,name=error,proto3" json:"error,omitempty"`
	// The encrypted data if encryption succeeded
	// (This will be an empty array if encryption failed)
	Ciphertext []byte `protobuf:"bytes,2,opt,name=ciphertext,proto3" json:"ciphertext,omitempty"`
}

func (x *EncryptionResponse) Reset() {
	*x = EncryptionResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_encryption_service_encryption_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EncryptionResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EncryptionResponse) ProtoMessage() {}

func (x *EncryptionResponse) ProtoReflect() protoreflect.Message {
	mi := &file_encryption_service_encryption_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EncryptionResponse.ProtoReflect.Descriptor instead.
func (*EncryptionResponse) Descriptor() ([]byte, []int) {
	return file_encryption_service_encryption_service_proto_rawDescGZIP(), []int{1}
}

func (x *EncryptionResponse) GetError() string {
	if x != nil {
		return x.Error
	}
	return ""
}

func (x *EncryptionResponse) GetCiphertext() []byte {
	if x != nil {
		return x.Ciphertext
	}
	return nil
}

// Represents an decryption request
type DecryptionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The data to be decrypted
	Ciphertext []byte `protobuf:"bytes,1,opt,name=ciphertext,proto3" json:"ciphertext,omitempty"`
}

func (x *DecryptionRequest) Reset() {
	*x = DecryptionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_encryption_service_encryption_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DecryptionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DecryptionRequest) ProtoMessage() {}

func (x *DecryptionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_encryption_service_encryption_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DecryptionRequest.ProtoReflect.Descriptor instead.
func (*DecryptionRequest) Descriptor() ([]byte, []int) {
	return file_encryption_service_encryption_service_proto_rawDescGZIP(), []int{2}
}

func (x *DecryptionRequest) GetCiphertext() []byte {
	if x != nil {
		return x.Ciphertext
	}
	return nil
}

// Represents an decryption response
type DecryptionResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The error message if decryption failed
	// (This will be an empty string if decryption succeeded)
	Error string `protobuf:"bytes,1,opt,name=error,proto3" json:"error,omitempty"`
	// The decrypted data if decryption succeeded
	// (This will be an empty array if decryption failed)
	Plaintext []byte `protobuf:"bytes,2,opt,name=plaintext,proto3" json:"plaintext,omitempty"`
}

func (x *DecryptionResponse) Reset() {
	*x = DecryptionResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_encryption_service_encryption_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DecryptionResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DecryptionResponse) ProtoMessage() {}

func (x *DecryptionResponse) ProtoReflect() protoreflect.Message {
	mi := &file_encryption_service_encryption_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DecryptionResponse.ProtoReflect.Descriptor instead.
func (*DecryptionResponse) Descriptor() ([]byte, []int) {
	return file_encryption_service_encryption_service_proto_rawDescGZIP(), []int{3}
}

func (x *DecryptionResponse) GetError() string {
	if x != nil {
		return x.Error
	}
	return ""
}

func (x *DecryptionResponse) GetPlaintext() []byte {
	if x != nil {
		return x.Plaintext
	}
	return nil
}

var File_encryption_service_encryption_service_proto protoreflect.FileDescriptor

var file_encryption_service_encryption_service_proto_rawDesc = []byte{
	0x0a, 0x2b, 0x65, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x2d, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2f, 0x65, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x05, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0x31, 0x0a, 0x11, 0x45, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x70, 0x6c, 0x61,
	0x69, 0x6e, 0x74, 0x65, 0x78, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x09, 0x70, 0x6c,
	0x61, 0x69, 0x6e, 0x74, 0x65, 0x78, 0x74, 0x22, 0x4a, 0x0a, 0x12, 0x45, 0x6e, 0x63, 0x72, 0x79,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a,
	0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x72,
	0x72, 0x6f, 0x72, 0x12, 0x1e, 0x0a, 0x0a, 0x63, 0x69, 0x70, 0x68, 0x65, 0x72, 0x74, 0x65, 0x78,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0a, 0x63, 0x69, 0x70, 0x68, 0x65, 0x72, 0x74,
	0x65, 0x78, 0x74, 0x22, 0x33, 0x0a, 0x11, 0x44, 0x65, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x63, 0x69, 0x70, 0x68,
	0x65, 0x72, 0x74, 0x65, 0x78, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x0a, 0x63, 0x69,
	0x70, 0x68, 0x65, 0x72, 0x74, 0x65, 0x78, 0x74, 0x22, 0x48, 0x0a, 0x12, 0x44, 0x65, 0x63, 0x72,
	0x79, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14,
	0x0a, 0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65,
	0x72, 0x72, 0x6f, 0x72, 0x12, 0x1c, 0x0a, 0x09, 0x70, 0x6c, 0x61, 0x69, 0x6e, 0x74, 0x65, 0x78,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x09, 0x70, 0x6c, 0x61, 0x69, 0x6e, 0x74, 0x65,
	0x78, 0x74, 0x32, 0x9f, 0x01, 0x0a, 0x11, 0x45, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69, 0x6f,
	0x6e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x44, 0x0a, 0x0b, 0x45, 0x6e, 0x63, 0x72,
	0x79, 0x70, 0x74, 0x44, 0x61, 0x74, 0x61, 0x12, 0x18, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e,
	0x45, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x19, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x6e, 0x63, 0x72, 0x79, 0x70,
	0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x44,
	0x0a, 0x0b, 0x44, 0x65, 0x63, 0x72, 0x79, 0x70, 0x74, 0x44, 0x61, 0x74, 0x61, 0x12, 0x18, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x44, 0x65, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69, 0x6f, 0x6e,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e,
	0x44, 0x65, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x22, 0x00, 0x42, 0x3b, 0x5a, 0x39, 0x74, 0x65, 0x6e, 0x73, 0x6f, 0x72, 0x77, 0x6f,
	0x72, 0x6b, 0x73, 0x2f, 0x64, 0x61, 0x6e, 0x2f, 0x65, 0x6e, 0x63, 0x72, 0x79, 0x70, 0x74, 0x65,
	0x64, 0x2d, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2d, 0x73, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x65, 0x2d,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x65,
	0x64, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_encryption_service_encryption_service_proto_rawDescOnce sync.Once
	file_encryption_service_encryption_service_proto_rawDescData = file_encryption_service_encryption_service_proto_rawDesc
)

func file_encryption_service_encryption_service_proto_rawDescGZIP() []byte {
	file_encryption_service_encryption_service_proto_rawDescOnce.Do(func() {
		file_encryption_service_encryption_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_encryption_service_encryption_service_proto_rawDescData)
	})
	return file_encryption_service_encryption_service_proto_rawDescData
}

var file_encryption_service_encryption_service_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_encryption_service_encryption_service_proto_goTypes = []interface{}{
	(*EncryptionRequest)(nil),  // 0: proto.EncryptionRequest
	(*EncryptionResponse)(nil), // 1: proto.EncryptionResponse
	(*DecryptionRequest)(nil),  // 2: proto.DecryptionRequest
	(*DecryptionResponse)(nil), // 3: proto.DecryptionResponse
}
var file_encryption_service_encryption_service_proto_depIdxs = []int32{
	0, // 0: proto.EncryptionService.EncryptData:input_type -> proto.EncryptionRequest
	2, // 1: proto.EncryptionService.DecryptData:input_type -> proto.DecryptionRequest
	1, // 2: proto.EncryptionService.EncryptData:output_type -> proto.EncryptionResponse
	3, // 3: proto.EncryptionService.DecryptData:output_type -> proto.DecryptionResponse
	2, // [2:4] is the sub-list for method output_type
	0, // [0:2] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_encryption_service_encryption_service_proto_init() }
func file_encryption_service_encryption_service_proto_init() {
	if File_encryption_service_encryption_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_encryption_service_encryption_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EncryptionRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_encryption_service_encryption_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EncryptionResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_encryption_service_encryption_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DecryptionRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_encryption_service_encryption_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DecryptionResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_encryption_service_encryption_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_encryption_service_encryption_service_proto_goTypes,
		DependencyIndexes: file_encryption_service_encryption_service_proto_depIdxs,
		MessageInfos:      file_encryption_service_encryption_service_proto_msgTypes,
	}.Build()
	File_encryption_service_encryption_service_proto = out.File
	file_encryption_service_encryption_service_proto_rawDesc = nil
	file_encryption_service_encryption_service_proto_goTypes = nil
	file_encryption_service_encryption_service_proto_depIdxs = nil
}
